import { observable, computed, toJS, action, decorate } from 'mobx'
import { filterTodos } from '../utils'

class TodoStore {
  todos = [];

  constructor(initialTodos, rootStore) {
    this.todos = initialTodos.todos || [];
    this.rootStore = rootStore;
  }

  /** Computed **/
  get filteredTodos() {
    return toJS(filterTodos(this.todos,
      this.rootStore.viewStore.query));
  }

  /** Actions **/
  add = (quadrant, text) => {
    this.todos.push({
      id: Date.now(),
      text: text,
      quadrant: quadrant
    });
  };

  setDone = (id) => {
    let todo = this.todos.find(t => t.id === id);
    todo.done = !todo.done;
  };

  delete = (id) => {
    let newTodos = this.todos.filter(todo => todo.id !== id);
    this.todos.replace(newTodos);
  };

  update = (id, text) => {
    let todo = this.todos.find(t => t.id === id);
    todo.text = text;
  }
}


export default decorate(TodoStore, {
  todos: observable,
  filteredTodos: computed,
  add: action,
  setDone: action,
  delete: action,
  update: action
});