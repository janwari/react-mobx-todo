import { computed, observable, toJS, action, decorate } from 'mobx'

class ViewStore {
  query = "";
  isEditing = {};

  constructor(initialTodos, rootStore) {
    this.isEditing = initialTodos.isEditing || {};
    this.rootStore = rootStore;
  }

  /** Computed **/
  get editing() {
    return toJS(this.isEditing);
  }

  /** Actions **/
  startEditing = id => {
    this.isEditing = {
      status: true,
      id: id
    }
  };

  endEditing = () => {
    this.isEditing = {
      status: false,
      id: ''
    }
  };

  executeSearch = query => {
    this.query = query;
  }
}


export default decorate(ViewStore, {
  query: observable,
  isEditing: observable,
  editing: computed,
  startEditing: action,
  endEditing: action,
  executeSearch: action
});
