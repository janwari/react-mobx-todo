import TodoStore from "./TodoStore";
import ViewStore from "./ViewStore";
import { loadState, saveState } from '../localStorage';
import { autorun, toJS } from 'mobx';

const initialState = loadState();

class Store {

  constructor() {
    this.todoStore = new TodoStore(initialState, this);
    this.viewStore = new ViewStore(initialState, this);
  }
}


const stores = new Store();

autorun(() => {
  saveState({
    todos: toJS(stores.todoStore.todos)
  })
});

export default stores;
