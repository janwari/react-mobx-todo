import React, { Component } from 'react';
import './App.css';
import MainContainer from "./components/containers/MainContainer";
import { Provider } from 'mobx-react';
import Footer from './components/presentational/Footer';
import stores from './stores';


class App extends Component {
  render() {
    return (
      <Provider {...stores}>
        <div className="App">
          <h1 className="App-title">Covey’s Time Management Quadrants</h1>
          <MainContainer />
          <Footer />
        </div>
      </Provider>
    );
  }
}

export default App;
