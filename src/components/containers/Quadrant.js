import React, { Component } from 'react'
import TodoList from '../presentational/TodoList'
import { observer, inject } from 'mobx-react';

class Quadrant extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.newTodoInput = React.createRef();
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.todoStore.add(this.props.quadrant,
      this.newTodoInput.current.value);
    this.newTodoInput.current.value = '';
  }

  render() {
    return (
      <div className="quadrant">
        <form onSubmit={this.handleSubmit}>
          <input
            name="newtodo"
            type="text"
            className="add-todo"
            placeholder="Add new Todo Item"
            ref={this.newTodoInput}
          />
        </form>
        
        <TodoList
          todos={this.props.todos}
          value={this.props.value}
          quadrant={this.props.quadrant} />
      </div>
    )
  }
}


export default inject('todoStore')(observer(Quadrant));
