import React, { Component } from 'react'
import EditTodoForm from './EditTodoForm';
import { observer, inject } from 'mobx-react';

class TodoItem extends Component {

  constructor(props) {
    super(props);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleDone = this.handleDone.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
  }

  handleDelete() {
    this.props.todoStore.delete(this.props.todo.id);
  }

  handleDone() {
    this.props.todoStore.setDone(this.props.todo.id);
  }

  handleEdit() {
    this.props.viewStore.startEditing(this.props.todo.id);
  }

  render() {
    const { status: isEditing, id: editTodoId } = this.props.viewStore.editing;
    const doneClassName = this.props.todo.done ? 'done-true' : 'done-false';
    const editingClassName = (isEditing && editTodoId === this.props.todo.id)
      ? 'editing-true' : 'editing-false';
    const classNames = [doneClassName, editingClassName].join(' ');

    return (
      <li>
        <span className={classNames}>
          <input
            type="checkbox"
            className="todo-done"
            checked={this.props.todo.done}
            onChange={this.handleDone}
          />
          <span
            className={doneClassName}
            onDoubleClick={this.handleEdit}>
            {this.props.todo.text}
          </span>
          <button onClick={this.handleDelete}>✖</button>
        </span>

        <EditTodoForm
          className={editingClassName}
          todo={this.props.todo}
          quadrant={this.props.quadrant}
        />
      </li>
    )
  }
}


export default inject('todoStore', 'viewStore')(observer(TodoItem));
