import React, { Component } from 'react'
import { observer, inject } from 'mobx-react';

class SearchForm extends Component {

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.props.viewStore.executeSearch(event.target.value);
  }
  
  render() {
    return (
      <div>
          <input 
            name="search" 
            type="text" 
            className="search-todos"
            placeholder="Search all Todo items" 
            onChange={this.handleChange}
          />
      </div>
    )
  }
}

export default inject('viewStore')(observer(SearchForm));

