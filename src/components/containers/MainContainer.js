import React, { Component } from 'react'
import CoveyQuadrants from "../presentational/CoveyQuadrants";
import { observer, inject } from 'mobx-react';

class MainContainer extends Component {
  render() {
    return (
      <React.Fragment>
        <CoveyQuadrants todos={this.props.todoStore.filteredTodos} />
      </React.Fragment>
    )
  }
}


export default inject('todoStore')(observer(MainContainer));
