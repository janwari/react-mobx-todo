import React, { Component } from 'react'

export default class Footer extends Component {
  render() {
    return (
      <div className="footer">
        <p>A sample React + MobX Todo app for my learning</p>
        <p>-- Jahangir Anwari --</p>
      </div>
    )
  }
}