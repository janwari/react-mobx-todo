import React, { Component } from 'react'
import Quadrant from '../containers/Quadrant'
import QuadrantLabels from "./QuadrantLabels";
import SearchForm from '../containers/SearchForm';

export default class CoveyQuadrants extends Component {
  render() {
    let quadrants = [1, 2, 3, 4];
    let quadrantColumns = quadrants.map((quadrant) => {
      let currentQuadrant = 'quadrant-' + quadrant;
      let quadrantTodos = this.props.todos.filter((todo) =>
        todo.quadrant === quadrant);

      return <Quadrant
        value={currentQuadrant}
        quadrant={quadrant}
        key={quadrant.toString()}
        todos={quadrantTodos}
      />
    });

    return (
      <div>
        <SearchForm />
        <QuadrantLabels />
        <div id="covey-quadrants">
          {quadrantColumns}
        </div>
      </div>
    )
  }
}
