import React, { Component } from 'react'

class QuadrantLabels extends Component {
  render() {
    return (
      <div>
        <div className="quadrant-label">
          <p className="urgent-quadrant-label urgent">Urgent</p>
          <p className="urgent-quadrant-label not-urgent">Not &nbsp;Urgent</p>
        </div>
        <div className="quadrant-label important">
          <p className="important-quadrant-label important">Important</p>
          <p className="important-quadrant-label not-important">Not &nbsp;Important</p>
        </div>
      </div>
    )
  }
}

export default QuadrantLabels;