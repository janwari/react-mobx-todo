import React, { Component } from 'react'
import TodoItem from '../containers/TodoItem'

export default class TodoList extends Component {
  render() {
    // Avoid using index as keys otherwise deleting an item
    // would not behave as expected. When doing edit item it 
    // may still refer to the deleted todo item. See below SO question
    // for more detail
    // https://stackoverflow.com/q/46477711
    const todoItems = this.props.todos.map((todo, index) =>
      <TodoItem
        key={todo.id}
        todo={todo}
        quadrant={this.props.quadrant}
      />
    );
    return (
      <ul>
        {todoItems}
      </ul>
    )
  }
}
