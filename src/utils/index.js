

export const filterTodos = (todos, query) => {
  return todos.filter((todo) => {
    return todo.text.toLowerCase().search(query.toLowerCase()) !== -1;
  });
};
